//
//  ViewController.m
//  HelloWorldGit
//
//  Created by Sai Prudhvi on 1/21/17.
//  Copyright © 2017 Sai Prudhvi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize message;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
    self.message.font = [UIFont fontWithName:@"Colleged" size:30];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)buttonPress:(id)sender {
    self.message.text = @"Sai Prudhvi";
}

- (IBAction)changeColor:(id)sender {
    self.message.textColor = [UIColor redColor];
}

- (IBAction)changeShadow:(id)sender {
    self.message.layer.shadowColor = [[UIColor yellowColor] CGColor];
    self.message.layer.shadowOpacity = 0.5;
    self.message.layer.shadowRadius = 3.0f;
    self.message.layer.shadowOffset = CGSizeMake(0, 2);
}

- (IBAction)alignLeft:(id)sender {
    self.message.textAlignment = NSTextAlignmentLeft;
}

- (IBAction)alignRight:(id)sender {
    self.message.textAlignment = NSTextAlignmentRight;

}

- (IBAction)alignCenter:(id)sender {
    self.message.textAlignment = NSTextAlignmentCenter;

}

- (IBAction)setFont:(id)sender {
    [self.message setFont:[UIFont fontWithName:@"Colleged" size:30]];
}

@end;
