//
//  AppDelegate.h
//  HelloWorldGit
//
//  Created by Sai Prudhvi on 1/21/17.
//  Copyright © 2017 Sai Prudhvi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

