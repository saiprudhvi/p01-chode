//
//  main.m
//  HelloWorldGit
//
//  Created by Sai Prudhvi on 1/21/17.
//  Copyright © 2017 Sai Prudhvi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
