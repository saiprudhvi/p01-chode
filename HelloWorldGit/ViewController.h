//
//  ViewController.h
//  HelloWorldGit
//
//  Created by Sai Prudhvi on 1/21/17.
//  Copyright © 2017 Sai Prudhvi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *message;
- (IBAction)buttonPress:(id)sender;
- (IBAction)changeColor:(id)sender;
- (IBAction)changeShadow:(id)sender;
- (IBAction)alignLeft:(id)sender;
- (IBAction)alignRight:(id)sender;
- (IBAction)alignCenter:(id)sender;
- (IBAction)setFont:(id)sender;

@end

